<!doctype html>  

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		
		<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
		
		<!-- Google Chrome Frame for IE -->
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		
		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo THEME_LIBRARY_URL ?>/images/favicon.ico">
				
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!--[if lt IE 9]>
			<script src="js/libs/html5shiv.js"></script>
		<![endif]-->

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		
	</head>
	
	<body>
	
		
		<h1>Welcome to the Div Starter Beta</h1>

		<h3>Description</h3>

		<p>Div Starter is a parent theme created specifically for developers in order to make WordPress theme development more streamlined. It is equipped with a SASS mobile first approach to front-end development, and includes helpful resources like <a target="_blank" href="http://www.advancedcustomfields.com/">ACF</a> (Advance Custom Fields) for custom solution within the WordPress dashboard. Among that there are helpful classes included that make things like <a target="_blank" href="http://codex.wordpress.org/Post_Types">Custom Post Types</a> a breeze (these are often referred to as modules within the Div Starter approach)</p>

		<h3>Getting Started</h3>
		<ol>
			<li><strong>Activate Child Theme</strong>
				<ul>
					<li>If you have not already installed a Div Starter child theme then the first step is to pick a theme, install it in the themes directory and activate it</li>
				</ul>
			</li>
			<li><strong>Brand the Child Theme</strong>
				<ul>
					<li>Edit the child theme style.css (name, description, etc)</li>
					<li><strong>NOTE:</strong> Do not change the template from 'div-starter'</li>
					<li>Edit the screenshot.png</li>
					<li>Edit the <strong>Developer Tools Page</strong>
						<em>(/wp-admin/admin.php?page=acf-options-developer-tools)</em>
					</li>
				</ul>	
			</li>
			<li><strong>Develop the Child Theme</strong></li>
				<ul>
					<li>Edit the template files in the child theme as needed and learn more from the <a target="_blank" href="http://starter.divtruth.com">Starter page documentation<a></li>
				</ul>
		</ol>

		<h3>Need More Help?</h3>
		Refer to the <a href="http://starter.divtruth.com">Div Starter page<a> for questions about the framework. From there you will find a contact form as well for any additional questions. If you discover a bug or have a feature request please post on the <a target="_blank" href="https://github.com/Xtremefaith/div-starter/issues">github page</a>

	</body>
</html>